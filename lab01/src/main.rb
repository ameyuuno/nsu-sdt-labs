# 
#   Project: Software Development Technology - Lab 01
#
#   Course: Software Development Technology
#       Novosibirsk State University
#       Department of Computer Science
#       Master Degree - Term 03
#
#
#   Created by yuukos on 2018-09-09 
#
#   GitHub: @yuukos
#


#
# Solution #1
#
# :: Using built-in ``repeated_permutation`` method of ``Array``
#
def generate_repeated_permutations_without_doubles(items, permutation_size)
  items.repeated_permutation(permutation_size).to_a.select {|permutation| !(1..permutation.length - 1).map {|i| permutation[i - 1] == permutation[i]}.any?}
end
# ===========


#
# Solution #2
#
# :: Using only simple built-in chain calls like ``reduce``, ``select``, etc.
#
def generate_repeated_permutations_without_doubles(items, permutation_size)
  (0..permutation_size - 1).reduce([[]]) {|permutations, _| permutations.map {|permutation| items.select {|item| item != permutation[-1]}.map {|item| permutation + [item]}}.flatten(1)}
end
# ===========


#
# Solution #3
#
# :: Using tail recursion
#
# This function is outside of ``generate_repeated_permutations_without_doubles`` because tail call optimization does
# not support optimization nested functions
def _build_permutations(permutations, items, step)
  return permutations if step == 0

  _build_permutations(permutations.map {|permutation| items.select {|item| item != permutation[-1]}.map {|item| permutation + [item]}}.flatten(1), items, step - 1)
end


def generate_repeated_permutations_without_doubles(items, permutation_size)
  _build_permutations([[]], items, permutation_size)
end


# This instruction enables TCO - Tail Call Optimization
RubyVM::InstructionSequence.compile_option = {
    tailcall_optimization: true,
    trace_instruction: false,
}
# ===========

def main(argv)
  if argv.length < 2 or argv.include?("-h") or argv.include?("--help")
    print_help
    return
  end

  characters = argv[0].chars
  n = argv[1].to_i

  generate_repeated_permutations_without_doubles(characters, n).each {|permutation| puts(permutation.join(""))}
end


def print_help
  usage = "
    Usage: ruby main.rb <characters> <length_of_generating_strings>

    Parameters:
    -----------
    * characters                    - characters which will be used to generate strings, enter them without any separators
    * length_of_generating_strings  - length of generating strings

    Example:
    --------
    $ ruby main.rb abc 2
    ab
    ac
    ba
    bc
    ca
    cb
    $
  "

  puts(usage)
end


if __FILE__ == $0
  main(ARGV)
end
